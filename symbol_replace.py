#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Symbol Replace - A (*failed*) attempt to replace symbols across files
# Does not work very well due to transform errors.
# An Inkscape 1.1+ extension
##############################################################################

import inkex

from lxml import etree
import sys, os
import uuid
from copy import deepcopy

# Unit conversions
conversions = {
    'in': 96.0,
    'pt': 1.3333333333333333,
    'px': 1.0,
    'mm': 3.779527559055118,
    'cm': 37.79527559055118,
    'm': 3779.527559055118,
    'km': 3779527.559055118,
    'Q': 0.94488188976378,
    'pc': 16.0,
    'yd': 3456.0,
    'ft': 1152.0,
    '': 1.0,  # Default px
}

def check_output_filepath(self, export_folder, base_filename):
    import os
    # Test Export Folder writable with known good filename
    test_filename = str(uuid.uuid1())
    if check_filepath_writable(self, export_folder, test_filename):
        None
    else:
        inkex.errormsg('Export Folder is *Not Writeable*')
        sys.exit()
    # Test Export Filename
    if check_filepath_writable(self, export_folder, base_filename):
        # inkex.errormsg('Export Folder is okay')
        export_filepath = os.path.join(export_folder, base_filename)
        return export_filepath
    else:
        inkex.errormsg('Base Filename is *invalid*')
        sys.exit()


def check_filepath_writable(self, folderpath, filename):
    import os

    filepath = os.path.join(folderpath, filename)

    try:
        try_file = open(filepath, 'w')
        try_file.close()
        os.remove(filepath)
        return True
    except:
        return False


def get_files_in_folder(self, folder, extension=None):
    filepath_list = []
    files = os.listdir(folder)
    if len(files) < 1:
        inkex.errormsg('No Files Found')
        return None

    for file in files:
        if file.endswith(extension):
            filepath_list.append(os.path.join(folder, file))

    return filepath_list

def svg_file_to_etree(self, svg_filepath):
    try:
        svg_etree = etree.parse(svg_filepath)
        return svg_etree
    except:
        inkex.errormsg(f'File Skipped - Unable to open svg file: {svg_filepath}')
        return False

class SymbolReplace(inkex.EffectExtension):

    def add_arguments(self, pars):
        # Input Folder
        pars.add_argument("--input_folder", type=str, dest="input_folder", default=None)
        # Output Folder
        pars.add_argument("--output_folder", type=str, dest="output_folder", default=None)

        # Id Source
        pars.add_argument("--id_source_radio", type=str, dest="id_source_radio", default='none')

        # Id List
        pars.add_argument("--target_id_str", type=str, dest="target_id_str", default='x')

        # Shape Unit Fix ( this is where the bounding box for some
        # objects in Inkscape 1.1 are returned in pixels instead of user units
        pars.add_argument("--shapeUnitFix", type=str, dest="shape_unit_fix")

    def effect(self):

        # Conversion factor
        found_units = self.svg.unit
        if self.options.shape_unit_fix == 'true':
            cf = conversions[found_units]
        else:
            cf = 1

        selection_list = self.svg.selection

        input_folder = self.options.input_folder

        if self.options.id_source_radio == 'selected':

            if len(selection_list) < 1:
                inkex.errormsg('Nothing Selected')
                sys.exit()

        input_file_list = get_files_in_folder(self, input_folder, extension='svg')

        # Exit if no input files found
        if len(input_file_list) < 1:
            inkex.errormsg('No input files found')
            sys.exit()


        for input_file in input_file_list:
            if os.path.isdir(input_file):
                None
            else:
                target_id = str(self.options.target_id_str)
                loaded_svg = inkex.load_svg(input_file)
                svg_element = loaded_svg.getroot()
                # inkex.errormsg(svg_element)
                if len(svg_element.xpath(f'//*[@id="{target_id}"]')) > 0:
                    found_element = svg_element.xpath(f'//*[@id="{target_id}"]')[0]
                    if found_element.TAG != 'symbol':
                        inkex.errormsg('Target is not a symbol !')
                        return
                    inkex.errormsg(f'found {target_id} in {svg_element} --- Filename: {input_file}')

                    # Test clone for positioning
                    test_clone = inkex.Use()
                    target_id_href = f'#{target_id}'
                    test_clone.set('xlink:href', target_id_href)
                    test_clone.set('id', 'test_clone')
                    svg_element.append(test_clone)

                    # Lets compare the on canvas bounding boxes of old symbol vs new symbol
                    old_bbox = test_clone.bounding_box()

                    # Lets replace the element
                    substitute_element = deepcopy(selection_list[0])
                    # Clear out the old element contents
                    for child in found_element.getchildren():
                        child.delete()
                    # Add the replacement element
                    found_element.append(substitute_element)

                    # Lets compare the on canvas bounding boxes of old symbol vs new symbol
                    new_bbox = test_clone.bounding_box()
                    # Remove the test object
                    test_clone.delete()
                    # Look at the differences in the bounding boxes
                    x_shift = old_bbox.left - (new_bbox.left / cf)
                    y_shift = old_bbox.top - (new_bbox.top / cf)

                    substitute_element.transform.add_matrix(1,0,0,1, x_shift, y_shift)

                    # Lets write to folder
                    output_folder = self.options.output_folder
                    new_filename = str(uuid.uuid4()) + '.svg'
                    if not check_filepath_writable(self, output_folder, new_filename):
                        inkex.errormsg('cannot write to output folder')
                        continue
                    new_svg_filepath = os.path.join(output_folder, new_filename)
                    with open(new_svg_filepath, 'w') as new_svg_file:
                        svg_text = svg_element.tostring().decode('utf-8')
                        new_svg_file.write(svg_text)

                else:
                    inkex.errormsg('Target Not Found in File')


if __name__ == '__main__':
    SymbolReplace().run()

